//
//  ViewController.swift
//  Drawing_Kit
//
//  Created by Amol Tamboli on 04/05/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import PencilKit

class ViewController: UIViewController {
    
    @IBOutlet weak var canvasView: PKCanvasView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCanvasView()
    }
    
    private func setupCanvasView(){
        if let window = view.window, let toolPicker = PKToolPicker.shared(for: window) {
            toolPicker.setVisible(true, forFirstResponder: canvasView)
            toolPicker.addObserver(canvasView)
            canvasView.becomeFirstResponder()
        }
    }
    
    @IBAction func btnImageTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ImageViewController") as? ImageViewController
        let img = UIGraphicsImageRenderer(bounds: canvasView.bounds).image { (_) in
            view.drawHierarchy(in: canvasView.bounds, afterScreenUpdates: true)
        }
        vc?.img = img
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func btnClearTapped(_ sender: Any) {
        canvasView.drawing = PKDrawing()
    }
    
}

