//
//  ImageViewController.swift
//  Drawing_Kit
//
//  Created by Amol Tamboli on 04/05/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var capturImg: UIImageView!
    var img = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        capturImg.image = img
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
